import uuid
import time
import threading


from hashlib import sha3_256

class User:
    def __init__(self, telegram_id, chat, bot, actual_name):
        self.telegram_id = telegram_id
        self.chat = chat
        self.actual_name = actual_name
        self.name = str(uuid.uuid4())  # random name for logging. means i wont be able to tell whos who
        self.messages = {}

        self.constructed_message = {}

        self.bot = bot

        # santa = person buying for them, elf = person they are buying for
        self.santa = None
        self.elf = None

        self.playing = False

    def __eq__(self, other):
        """ Consider two objects equal if they have the same telegram id and is in the same chat """
        return self.telegram_id == other.telegram_id and self.chat == other.chat

    def __repr__(self):
        """ Use the obscured name as the representation of the object"""
        return f"<User:{self.name}>"

    def __hash__(self):
        # will always be identical between runs
        return hash(self.telegram_id) + hash(self.actual_name) + hash(self.name)

    def consistent_hash(self):
        return sha3_256(f"{self.telegram_id}-_-{self.actual_name}".encode()).hexdigest()

    def add_santa(self, santa):
        assert isinstance(santa, User)

        self.santa = santa

    def add_elf(self, elf):
        assert isinstance(elf, User)

        self.elf = elf

    def update_playing(self):
        if self.playing:
            self.playing = False
            message = "You are no longer playing"
        else:
            self.playing = True
            message = "You are now signed up"

        self.send_message(message)

    ###### Messaging functions ######

    def get_current_message(self, recipient=None):
        return self.constructed_message

    def add_to_message(self, var, content):
        self.constructed_message[var] = content

    def construct_message(self):
        recep = self.constructed_message["Recipient"]

        if recep == "Santa":
            user_from = "elf"
        elif recep == "Elf":
            user_from = "santa"
        else:
            user_from = f"something went wrong here oops. recipient wasn't 'Santa' or 'Elf', it was {recep}"

        text = f"Message from your {user_from}:\n\n{self.constructed_message['Text']}"
        return text, self.constructed_message["Priority"]

    def clear_message(self):
        self.constructed_message = {}

    def message_history(self, message_id, text):
        self.messages[message_id] = text
        print(f"List of messages for {self}: {self.messages}")

    def send_message(self, message):
        self.bot.send_message(self.telegram_id, message)

    def send_elf(self, message, delay=0):
        if self.elf is None:
            self.send_message("You do not currently have a person assigned")

        timer = threading.Timer(delay, self.send_delay, kwargs={"recepient": self.elf,
                                                                "message": message})
        timer.start()

    def send_santa(self, message, delay=0):
        if self.santa is None:
            self.send_message("You do not currently have a person assigned lol")
        timer = threading.Timer(delay, self.send_delay, kwargs={"recepient": self.santa,
                                                                "message": message})
        timer.start()

    def send_delay(self, recepient, message):
        recepient.send_message(message)
        message = message.replace("\n", "     ")
        self.send_message(f"Message '{message}' sent.")
