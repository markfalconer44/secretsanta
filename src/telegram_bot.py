import sys
import math

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ForceReply
import random
import re
import os

import pathlib

top_dir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(top_dir)

from src.user import User
from src.solver.node import Node
from src.solver.solver import HeldKarp

def received_message(func):
    def handler(self, update, context, *args, **kwargs):
        user_id = update.effective_user.id
        user_name = f"{update.effective_user.first_name} {update.effective_user.last_name}"
        chat = update.effective_chat
        user_obj = User(telegram_id=user_id, chat=chat.id, bot=self.bot, actual_name=user_name)
        if user_obj in self.users:
            for x in self.users:
                if x == user_obj:
                    user_obj = x
                    break
        else:
            self.bot.send_message(self.owner_id, f"{user_obj.actual_name} has messaged the bot!")
            self.users.append(user_obj)

        message = update.effective_message
        message_text = message.text
        message_id = message.message_id
        user_obj.message_history(message_id, message_text)

        kwargs["update"] = update

        return func(self=self, user_obj=user_obj, *args, **kwargs)
    return handler

class TelegramBot:
    def __init__(self, bot_token, owner):
        self.updater = Updater(bot_token, use_context=True)
        self.bot = self.updater.bot
        self.owner_id = owner
        self.users = []
        self.blocks = []
        if self.previous_game():
            for user in self.users:
                self.bot.send_message(user.telegram_id,
                                      "Bot has just restarted. Please resend any scheduled messages that hadn't sent.")
        else:
            self.bot.send_message(self.owner_id,
                                  "Bot has started with no previous game.")

    def previous_game(self):
        """ Check if a previous game was in progress. Return to that if so"""
        if not os.path.exists('bot_files/route.txt'):
            return False

        with open('bot_files/route.txt', 'r') as route_file:
            whole_file = route_file.readline()  # entire file is written on one line

        hashed_route = whole_file.split("\u0000")[:-1]  # drop the last one as its empty

        users_hash = {}
        with open('bot_files/users.txt', 'r') as users_file:
            lines = users_file.readlines()
            for line in lines:
                line = line.replace("\n", "")
                name, telegram_id, chat = line.split("\u0000")
                telegram_id = int(telegram_id)
                chat = int(chat)
                user_obj = User(telegram_id=telegram_id, chat=chat, bot=self.bot, actual_name=name)
                self.users.append(user_obj)
                users_hash[user_obj.consistent_hash()] = user_obj

        for user in self.users:
            user_hash = user.consistent_hash()
            user_index = hashed_route.index(user_hash)
            if user_index > 0:
                santa_hash = hashed_route[user_index-1]
            else:
                santa_hash = hashed_route[-1]

            if user_index < (len(hashed_route)-1):
                elf_hash = hashed_route[user_index+1]
            else:
                elf_hash = hashed_route[0]

            user.add_santa(users_hash[santa_hash])
            user.add_elf(users_hash[elf_hash])

        return True


    def run(self):
        dp = self.updater.dispatcher

        dp.add_handler(CommandHandler("start", self.start))
        dp.add_handler(CommandHandler("message", self.construct_message))
        dp.add_handler(CommandHandler("start_game", self.start_game))
        dp.add_handler(CommandHandler("block", self.block))
        dp.add_handler(CommandHandler("blocks", self.get_blocks))
        dp.add_handler(CommandHandler("clear", self.clear_users))
        dp.add_handler(CommandHandler("users", self.list_of_users))

        dp.add_handler(CallbackQueryHandler(self.handle_callback))

        dp.add_handler(MessageHandler(Filters.text, self.text_log))

        print("Starting... ")
        self.updater.start_polling()

        self.updater.idle()

    @received_message
    def clear_users(self, user_obj, update):
        if user_obj.telegram_id == self.owner_id:
            self.users = []
            self.blocks = []
            os.remove("bot_files/route.txt")
            os.remove("bot_files/users.txt")
            update.effective_message.reply_text(f"Previous game cleared. "
                                                f"User list is {self.users}\nBlock list is {self.blocks}")
    @received_message
    def list_of_users(self, user_obj, update):
        if user_obj.telegram_id == self.owner_id:
            message = "List of current users: \n\n"
            for user in self.users:
                message += f"{user.actual_name}\n"

            update.effective_message.reply_text(message)


    @received_message
    def text_log(self, user_obj, update):
        """ Will log any message received"""
        if update.effective_message.reply_to_message["text"] == "Enter the message you want to send.":
            # this seems very messy. surely theres a better way than this?
            text = update.effective_message.text
            user_obj.add_to_message("Text", text)
            self.construct_message(update=update, context=None)
            return
        if update.effective_message.reply_to_message["text"] == "Your parcel is due today <<insert time>>\n\nEnter the time range of the message.":
            text = update.effective_message.text.strip()
            user_obj.add_to_message("Text", f"Your parcel is due today {text}")
            self.construct_message(update=update, context=None)
            return
        print(update)
        pass

    @received_message
    def start(self, user_obj, update):

        user_obj.send_message("Welcome to the bot.")

    @received_message
    def construct_message(self, user_obj, update):
        if user_obj.elf is None:
            update.effective_message.reply_text("The game hasnt started yet.")
            return

        curr_messasge = user_obj.get_current_message()
        keyboard = []
        buttons = {"Recipient": "", "Text": "", "Priority": ""}
        completed = True
        for button in buttons.keys():
            text = f"{button}"
            if button in curr_messasge.keys():
                text += f": {curr_messasge[button]} ✅"
            else:
                completed = False
                text += f": ❌"

            actual_button = InlineKeyboardButton(text, callback_data=button.lower())
            keyboard.append([actual_button])

        if completed:
            message = "Message is ready to send!"
            keyboard.append([InlineKeyboardButton("Preview", callback_data='preview')])
        else:
            message = "Lets build a message."
        keyboard.append([InlineKeyboardButton("Cancel", callback_data='message_cancel')])

        self.bot.send_message(user_obj.telegram_id,
                              message,
                              reply_markup=InlineKeyboardMarkup(keyboard))

    def message_priority(self, user_obj, update):
        keyboard = [[InlineKeyboardButton("Immediate", callback_data='priority_immediate'),
                     InlineKeyboardButton("High (0-10 mins)", callback_data='priority_high')],
                    [InlineKeyboardButton("Normal (0-60 mins)", callback_data='priority_normal'),
                     InlineKeyboardButton("Low (0-12 hours)", callback_data='priority_low')]]

        update.effective_message.reply_text("How important is the message? A random amount of time within the limits will be selected",
                                            reply_markup=InlineKeyboardMarkup(keyboard))

    def message_recipient(self, user_obj, update):
        keyboard = [[InlineKeyboardButton("Santa", callback_data='message_recipient_santa')],
                    [InlineKeyboardButton(f"Elf ({user_obj.elf.actual_name})", callback_data='message_recipient_elf')]]

        update.effective_message.reply_text("Who is this message going to?",
                                            reply_markup=InlineKeyboardMarkup(keyboard))

    def message_text(self, user_obj, update):
        keyboard = [[InlineKeyboardButton("Your parcel is due today <<insert time>>", callback_data='message_text_due')],
                    [InlineKeyboardButton(f"Can you give me some ideas for what you want?", callback_data='message_text_ideas')],
                    [InlineKeyboardButton(f"Custom", callback_data='message_text_custom')]]

        update.effective_message.reply_text("What do you want to send?",
                                            reply_markup=InlineKeyboardMarkup(keyboard))

    def message_text_custom(self, user_obj, update):
        update.effective_message.reply_text("Enter the message you want to send.", reply_markup=ForceReply())

    def message_text_due_time(self, user_obj, update):
        update.effective_message.reply_text("Your parcel is due today <<insert time>>\n\nEnter the time range of the message.", reply_markup=ForceReply())

    def preview_message(self, user_obj, update):
        text, priority = user_obj.construct_message()
        recipient = user_obj.get_current_message()["Recipient"]
        keyboard = [[InlineKeyboardButton("Send", callback_data='preview_send')],
                    [InlineKeyboardButton("Edit", callback_data='preview_edit')]]

        update.effective_message.reply_text(f"Message to be sent to your {recipient.lower()} with {priority} priority:\n\n\n'{text}'",
                                            reply_markup=InlineKeyboardMarkup(keyboard))

    def message_send(self, user_obj, update):
        text, priority = user_obj.construct_message()
        delay = 0
        if priority == "Urgent":
            delay = 0
        elif priority == "High":
            # 0 and 10 mins
            delay = random.randint(0, 10)
        elif priority == "Normal":
            # 0 and 60 mins
            delay = random.randint(0, 60)
        elif priority == "Low":
            # 0 and 12 hours
            delay = random.randint(0, 60*12)

        delay_str = ""

        delay_hr = int(math.floor(delay / 60))
        delay_min = delay - (delay_hr*60)

        if delay_hr == 1:
            delay_str = f"{delay_hr} hour "
        elif delay_hr > 1:
            delay_str = f"{delay_hr} hours "

        if delay_min == 1:
            delay_str += f"{delay_min} minute"
        elif delay_min > 1:
            delay_str += f"{delay_min} minutes"

        update.effective_message.reply_text(f"The following message will be sent in {delay_str}:\n\n{text}")

        if user_obj.get_current_message()["Recipient"] == "Elf":
            user_obj.send_elf(text, delay*60)
        elif user_obj.get_current_message()["Recipient"] == "Santa":
            user_obj.send_santa(text, delay*60)

    @received_message
    def get_logs(self, user_obj, update):
        print("Return logs? Maybe obscure the chats or even just the current user")

    @received_message
    def playing(self, user_obj, update):
        user_obj.update_playing()

    @received_message
    def start_game(self, user_obj, update):
        print("Received an attempt to start game")
        if user_obj.telegram_id == self.owner_id:
            # randomise order of self.users
            random_user_list = random.sample(self.users, k=len(self.users))

            update.effective_message.reply_text(f"Starting the game with users: {random_user_list}")
            print(f"Starting the game with {len(random_user_list)} users")
            nodes = {}

            # create a node for each user
            for user in random_user_list:
                node_user = Node(0, 0, name=user.name)
                nodes[user] = node_user

            # add connections between all users with low weights
            for user in nodes.values():
                for o_user in nodes.values():
                    if o_user != user:
                        user.add_connection(o_user, 1)

            # alter the connection for any blocks to very high. this allows it to be still possible but unlikely
            high_weight = 1000000
            for block in self.blocks:
                user_a, user_b = block
                a_node = nodes[self.find_user(user_a)]
                b_node = nodes[self.find_user(user_b)]
                a_node.add_connection(b_node, high_weight)

            list_of_nodes = list(nodes.values())
            solver = HeldKarp(list_of_nodes)
            solution, distances = solver.solve()

            for i, node in enumerate(solution):
                nodes_keys = list(nodes.keys())
                nodes_values = list(nodes.values())
                node_index = nodes_values.index(node)
                user_obj = nodes_keys[node_index]

                if i > 0:
                    santa_node = solution[i-1]
                else:
                    santa_node = solution[-1]

                santa_node_index = nodes_values.index(santa_node)
                santa = nodes_keys[santa_node_index]

                if i < (len(nodes_keys)-1):
                    elf_node = solution[i+1]
                else:
                    elf_node = solution[0]

                elf_node_index = nodes_values.index(elf_node)
                elf = nodes_keys[elf_node_index]

                user_obj.add_santa(santa)
                user_obj.add_elf(elf)

                user_obj.send_message(f"You are buying for {elf.actual_name}!")

            print(f"Route: {solution}. Weight: {distances}")

            # save route and users info
            with open('bot_files/route.txt', 'w+') as route_file:
                for node in solution:
                    node_index = list(nodes.values()).index(node)
                    user_obj = list(nodes.keys())[node_index]

                    route_file.write(f"{user_obj.consistent_hash()}\u0000")

            with open('bot_files/users.txt', 'w+') as users_file:
                for user in random_user_list:
                    users_file.write(f"{user.actual_name}\u0000{user.telegram_id}\u0000{user.chat}\n")



    @received_message
    def block(self, user_obj, update):
        self.block_users(user_obj, update, first=None)

    def block_users(self, user_obj, update, first=None):
        if user_obj.telegram_id == self.owner_id:

            # use telegram ids not names in backend, keep showing names in ui
            if first is None:
                first_user = None
                message = "Select the first user."
            else:
                first_user = self.find_user(int(first))
                message = f"Select who {first_user.actual_name} can't buy for "

            keyboard = []
            users = self.users

            for user in users:
                # omit chosen person from list
                # todo: omit already chosen selections
                if first is not None:
                    if first_user == user:
                        continue
                name = user.actual_name
                text = f"{name}"
                valid = True
                for block in self.blocks:
                    if first is not None:
                        user_a, user_b = block
                        user_a_obj = self.find_user(user_a)
                        user_b_obj = self.find_user(user_b)
                        if user_a_obj == first_user and user_b_obj == user:
                            valid = False

                if not valid:
                    # ignore any current blocks
                    continue
                if first is None:
                    actual_button = InlineKeyboardButton(text, callback_data=f"block_one_{user.telegram_id}")
                else:
                    actual_button = InlineKeyboardButton(text, callback_data=f"block_two_{first}_{user.telegram_id}")
                keyboard.append([actual_button])

            if len(keyboard) == 0:
                self.bot.send_message(user_obj.telegram_id,
                                      f"There is no one for {first_user.actual_name} to buy for")
            else:
                self.bot.send_message(user_obj.telegram_id,
                                      message,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

    @received_message
    def get_blocks(self, user_obj, update):
        block_list = "List of all blocks:\n\n"
        for block in self.blocks:
            user_a, user_b = block
            user_a_obj = self.find_user(user_a)
            user_b_obj = self.find_user(user_b)

            block_list += f"{user_a_obj.actual_name} -> {user_b_obj.actual_name}\n"

        if len(self.blocks) == 0:
            block_list = "There is no blocks in place"

        update.effective_message.reply_text(block_list)

    @received_message
    def handle_callback(self, user_obj, update):
        query = update.callback_query
        target = query.data
        self.bot.deleteMessage(
            chat_id=query.message.chat.id,
            message_id=query.message.message_id
        )

        print(target)


        if target == "priority":
            # user is building a message and wants to select the priority
            self.message_priority(user_obj, update)
        elif target == "recipient":
            # user is building a message and wants to select the recipient
            self.message_recipient(user_obj, update)
        elif target == "text":
            self.message_text(user_obj, update)
        elif target == "preview":
            self.preview_message(user_obj, update)
        elif target == "priority_immediate":
            user_obj.add_to_message("Priority", "Urgent")
            self.construct_message(update=update, context=None)
        elif target == "priority_high":
            user_obj.add_to_message("Priority", "High")
            self.construct_message(update=update, context=None)
        elif target == "priority_normal":
            user_obj.add_to_message("Priority", "Normal")
            self.construct_message(update=update, context=None)
        elif target == "priority_low":
            user_obj.add_to_message("Priority", "Low")
            self.construct_message(update=update, context=None)
        elif target == "message_recipient_santa":
            user_obj.add_to_message("Recipient", "Santa")
            self.construct_message(update=update, context=None)
        elif target == "message_recipient_elf":
            user_obj.add_to_message("Recipient", "Elf")
            self.construct_message(update=update, context=None)
        elif target == "message_text_due":
            self.message_text_due_time(user_obj, update)
        elif target == "message_text_ideas":
            user_obj.add_to_message("Text", "Can you give me some ideas for what you want?")
            self.construct_message(update=update, context=None)
        elif target == "message_text_custom":
            self.message_text_custom(user_obj, update)
        elif target == "preview_send":
            self.message_send(user_obj, update)
            user_obj.clear_message()
        elif target == "message_cancel":
            user_obj.clear_message()
        elif target == "preview_edit":
            self.construct_message(update=update, context=None)
        elif target.startswith("block_one_"):
            # user has selected the first block
            first_block = re.findall("block_one_(.*)", target)
            self.block_users(user_obj, update, first=first_block[0])
        elif target.startswith("block_two_"):
            # user has selected the second block
            first_block, second_block = re.findall("block_two_(.[^_]*)_(.*)", target)[0]
            self.blocks.append((first_block, second_block))
            first_user = self.find_user(first_block).actual_name
            second_user = self.find_user(second_block).actual_name
            update.effective_message.reply_text(f"Block added between {first_user} and {second_user}")
        else:
            user_obj.send_message(f"How are you asking about {target}?")

    def find_user(self, telegram_id):
        if isinstance(telegram_id, str):
            telegram_id = int(telegram_id)
        for user in self.users:
            if telegram_id == user.telegram_id:
                return user


if __name__ == "__main__":
    token = sys.argv[1]
    owner = int(sys.argv[2])

    bot = TelegramBot(bot_token=token, owner=owner)
    bot.run()
