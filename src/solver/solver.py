import itertools
from datetime import datetime
import math

"""
This entire file is taken from my 5th year project, please dont find bugs here xx
"""


class Dijkstra:
    def __init__(self, nodes):
        self.nodes = nodes
        self.all_combs = []

    def solve(self, start, goal):
        """ Dijkstra algorithm implementation """
        # Initialise variables
        print("Finding route using Dijkstra")
        route = []
        unvisited = []
        dis = {start: 0}
        prev = {goal: None}

        # Set distance of each node to infinity
        for node in self.nodes:
            unvisited.append(node)
            dis[node] = math.inf

        dis[start] = 0
        prev[start] = None

        current_node = start

        at_goal = False
        count = 0
        goal_x, goal_y = goal.get_coords()
        # Loop until at goal
        while not at_goal and count < math.pow(len(self.nodes), 2):
            connections = current_node.get_connections()
            for neighbour in current_node.get_connections():
                if neighbour in unvisited:
                    dis_to = connections[neighbour]
                    new_dis = dis[current_node] + int(float(dis_to))
                    # is this distance smaller than the one already possible?
                    if new_dis < dis[neighbour]:
                        dis[neighbour] = new_dis
                        prev[neighbour] = current_node

            # current node is visited, remove it from unvisited
            if unvisited.__contains__(current_node):
                unvisited.remove(current_node)

            new_dis = math.inf
            next_node = current_node

            # Look at all unvisited nodes to pick the one with the smallest distance
            for potential_node in unvisited:
                if dis[potential_node] < new_dis:
                    next_node = potential_node
                    new_dis = dis[potential_node]

            current_node = next_node

            # is this the goal?
            x, y = current_node.get_coords()
            if x == goal_x  and y == goal_y:
                at_goal = True

            count += 1

        prv = current_node
        # add node to route
        while prv is not None:
            route.insert(0, prv)
            prv = prev[prv]

        return route, dis

    def get_nodes(self):
        return self.nodes

class HeldKarp:
    def __init__(self, nodes):
        self.nodes = nodes
        self.all_combs = []

    def held_karp(self, nodes):
        print("About to start solving.")
        # check all nodes have connections, otherwise there is definitely no valid route
        for node in nodes:
            if len(node.get_connections()) == 0:
                print("Node {} is missing connections. No valid route will be found".format(node.get_name()))
                raise NoValidRouteException


        # starting node doesnt matter
        start = nodes[0]

        # all other nodes
        others = nodes[1:]

        all = nodes.copy()
        # weighted connections
        all_combs = []
        prev_weights = {}
        prev_nodes = {}

        # debug
        count = 0

        # generate all subsets
        print("Generating combinations")
        total_size = int(math.pow(2, len(others)) - 1)
        for i in range(1, len(others) + 1):
            data = list(itertools.combinations(others, i))
            for d in data:
                all_combs.append(list(d))

        for set_data in all_combs:
            set_names = [i.get_name() for i in set_data]
            set_tuple = tuple(set_names)
            prev_weights[set_tuple] = {}
            prev_nodes[set_tuple] = {}
            for node in set_data:
                count += 1
                # remove current element, find subset
                subset = set_data.copy()
                subset.remove(node)
                subset_names = [i.get_name() for i in subset]
                subset_tuple = tuple(subset_names)
                if not subset:
                    sub_names = ["empty"]
                else:
                    sub_names = [i.get_name() for i in subset]
                # print("{} Tuple: {}".format(sub_names, subset_tuple))

                # get all weights for this subset with the last node travelled to
                if subset_tuple in prev_weights.keys():
                    weights = prev_weights[subset_tuple]
                else:
                    # only 1 node in
                    weights = {start: 0.0}

                # go through all possible subset variations
                for prev_node in weights.keys():
                    count += 1
                    conns = prev_node.get_connections()
                    # what is the weight from the last node to the current one
                    if node in conns.keys():
                        weight = conns[node]
                    else:
                        weight = math.inf

                    # how much was the weight to get to the last node?
                    running_weight = weights[prev_node]

                    # add new weight
                    new_weight = weight + running_weight

                    # have we already found a way to get to this node?
                    if node in prev_weights[set_tuple].keys():
                        prev_weight = prev_weights[set_tuple][node]
                    else:
                        prev_weight = math.inf

                    # ensure that we find the shortest route to this node.
                    if new_weight <= prev_weight:
                        prev_weights[set_tuple][node] = new_weight
                        prev_nodes[set_tuple][node] = prev_node.get_name()
                        # print("")


                # debug stuff
                # set_names = [i.get_name() for i in set_data]
                # print(str(node.get_name()) + " : " + str(set_names) + " : " + str(prev_weights[set_tuple][node]))

            # print("")

        # go back to start
        subset = all_combs[-1].copy()
        subset_names = [i.get_name() for i in subset]
        others_tuple = tuple(subset_names)
        weights = prev_weights[others_tuple].copy()
        all_names = [i.get_name() for i in all]
        all_tuple = tuple(all_names)
        prev_weights[all_tuple] = {}
        prev_nodes[all_tuple] = {}
        for prev_node in weights.keys():
            count += 1
            conns = prev_node.get_connections()
            # what is the weight from the last node to the current one
            if start in conns.keys():
                weight = conns[start]
            else:
                weight = math.inf

            # how much was the weight to get to the last node?
            running_weight = weights[prev_node]

            # add new weight
            new_weight = weight + running_weight

            # have we already found a way to get to this node?
            if start in prev_weights[all_tuple].keys():
                prev_weight = prev_weights[all_tuple][start]
            else:
                prev_weight = math.inf

            # ensure that we find the shortest route to this node.
            if new_weight <= prev_weight:
                prev_weights[all_tuple][start] = new_weight
                prev_nodes[all_tuple][start] = prev_node.get_name()



        print("Iterations run {}".format(count))
        if min(prev_weights[all_tuple].values()) == math.inf:
            print("There was no valid route found which goes to each junction once")
            raise NoValidRouteException

        set_names = [i.get_name() for i in subset]
        # print(str(start.get_name()) + " : " + str(set_names) + " : " + str(prev_weights[all_tuple]))

        full = others.copy()
        route = [start]
        last_node = route[-1]
        while len(full) > 0:
            count += 1
            # create tuple for dict key
            curr_tuple = tuple([i.get_name() for i in full])
            # get all possible nodes
            poss_nodes = prev_nodes[curr_tuple]
            min_val = math.inf
            for node in poss_nodes:
                count += 1
                weight = prev_weights[curr_tuple][node]
                if last_node in node.get_connections():
                    weight += node.get_connections()[last_node]
                else:
                    weight = math.inf
                # find node which creates smallest weight
                if weight < min_val:
                    min_val = weight
                    new_last = node
            # setup next loop
            last_node = new_last
            full.remove(new_last)
            route.append(new_last)
        # end at the starting node
        route.append(start)

        weight = 0.0
        last_node = route[-1]
        actual_route = []
        for node in reversed(route[:-1]):
            count += 1
            # print(node.get_name(), end="->")
            weight += last_node.get_connections()[node]
            last_node = node
            actual_route.append(node)
        #

        print("Iterations run {}".format(count))
        # print("\b\b")
        # print("Routes weight is {}, should be {}".format(weight, str(prev_weights[all_tuple][start])))
        return actual_route, weight

    def solve(self):
        nodes = self.nodes
        try:
            return self.held_karp(self.nodes)

        except NoValidRouteException:
            print("trying to add nodes")

        routes = {}
        for i in range(5):
            print(f"adding new nodes {i} times.")
            for n in nodes:
                new_nodes = nodes.copy()
                for _ in range(i):
                    new_nodes.append(n)
                try:
                    route, weight = self.held_karp(new_nodes)
                    routes[weight] = route
                    print(f"Valid route found going through {n.get_name()} {i} extra times. Weight {weight}")
                except NoValidRouteException:
                    pass
                    # print("failed with adding node {}  (list {})".format(n.get_name(), new_nodes))

            if len(routes.keys()) > 0:
                print("valid route found")
                for weight in sorted(routes):
                    return routes[weight], weight
            else:
                print("still cant find a route. ")

class NoValidRouteException(Exception):
    """ Exception raised when there is no valid route to be found. """
    pass
