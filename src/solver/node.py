class Node:
    def __init__(self, x, y, name="node"):
        assert type(x) is int, "x must be an integer"
        assert type(y) is int, "y must be an integer"
        assert x >= 0, "x must be positive"
        assert y >= 0, "y must be positive"

        self.name = name.rstrip()
        self.x_coord = x
        self.y_coord = y
        self.connections = {}

    def __eq__(self, other):
        return self.x_coord == other.x_coord and self.y_coord == other.y_coord and self.name == other.name

    def __hash__(self):
        return hash(self.x_coord) + hash(self.y_coord) + hash(self.name)

    # Setters #
    def add_connection(self, node, weight):
        assert type(node) is Node, "node must be of type Node"
        assert type(weight) in [int, float], "Weight must be an int or float"
        assert weight > 0, "Weight must be greater than 0"

        # make weight a float
        if type(weight) is int:
            weight = float(weight)

        # does connection already exists?
        if node in self.connections.keys():
            # has there been any change to the weight?
            if self.connections[node] == weight:
                print("Connection already exists and weight is the same")
                return False

        # add weight
        self.connections[node] = weight

        # print("Connection made between {} ({},{}) and {} ({},{}) with a weight of {}".format(self.name, self.x_coord,
        #                                                                              self.y_coord, node.name,
        #                                                                              node.x_coord, node.y_coord,
        #                                                                              weight))
        return True

    def remove_connection(self, node):
        if node in self.connections.keys():
            del self.connections[node]

    # Getters #
    def get_connections(self):
        return self.connections

    def get_coords(self):
        return self.x_coord, self.y_coord

    def get_name(self):
        return self.name

    def describe(self):
        return "{},{}".format(self.x_coord, self.y_coord)
